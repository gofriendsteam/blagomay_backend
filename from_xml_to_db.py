import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blagomay_backend.settings")
SECRET_KEY = os.environ.get('SECRET_KEY')
django.setup()

import xml.dom.minidom
from news.models import News
import htmlmin
from bs4 import BeautifulSoup
from yandex_translate import YandexTranslate
import re
from django.core.exceptions import ObjectDoesNotExist
API_KEY = 'trnsl.1.1.20191204T102758Z.24e99067fee17d98.4daec715e795582fd0001715539f08803851ed08'
translate = YandexTranslate(API_KEY)


def get_translation(list_uk_text, title, to_lang='en'):
    new_list = []
    for uk_origin in list_uk_text:
        en_element = translate.translate(uk_origin, 'uk-{}'.format(to_lang))['text'][0]
        new_list.append(en_element)

    return {
        'origin_uk': list_uk_text,
        '{}'.format(to_lang): new_list,
        'title_{}'.format(to_lang): translate.translate(title, 'uk-{}'.format(to_lang))['text'][0]
    }


def multiple_replace(string, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in sorted(rep_dict, key=len, reverse=True)]), flags=re.DOTALL)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)


def read_file(filename=""):
    file = xml.dom.minidom.parse(filename)
    title_objects = file.getElementsByTagName('item')
    for index, article in enumerate(title_objects):
        try:
            content = article.getElementsByTagName('content:encoded')[0].firstChild.data
            post_date = article.getElementsByTagName('wp:post_date')[0].firstChild.data
            title = article.getElementsByTagName('title')[0].firstChild.data
            lang = article.getElementsByTagName('link')[0].firstChild.data
            try:
                News.objects.get(title=title)
                continue
            except ObjectDoesNotExist as e:
                print(e)
                if content:
                    if 'uk' in lang:
                        soup = BeautifulSoup(htmlmin.minify(content), 'html.parser')
                        text = soup.find_all(text=True)
                        en_version = get_translation(list(text), title=title, to_lang='en')
                        ru_version = get_translation(list(text), title=title, to_lang='ru')
                        content_uk = str(htmlmin.minify(content))
                        content_ru = str(htmlmin.minify(content))
                        dict_from_uk_to_en = dict(zip(en_version['origin_uk'], en_version['en']))
                        dict_from_uk_to_ru = dict(zip(en_version['origin_uk'], ru_version['ru']))
                        for x, y in dict_from_uk_to_en.items():
                            if x != ' ':
                                content_uk = content_uk.replace(x, y)
                        for x, y in dict_from_uk_to_ru.items():
                            if x != ' ':
                                content_ru = content_ru.replace(x, y)
                        title_ru = ru_version['title_ru']
                        title_en = en_version['title_en']
                        News.objects.create(title=title,
                                            title_en=title_en,
                                            title_ru=title_ru,
                                            title_uk=title,
                                            content=content,
                                            content_en=content_uk,
                                            content_ru=content_ru,
                                            content_uk=content,
                                            pub_date=post_date)
                        print('Created!!')
        except Exception as e:
            print(e)
            continue


def main():
    read_file(os.path.join('./', 'archive_news.xml'))


if __name__ == '__main__':
    main()
