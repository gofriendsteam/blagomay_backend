import os
import uuid
from liqpay.liqpay3 import LiqPay


def get_liqpay_data_and_signature(request, **params):
    liqpay = LiqPay(
        public_key=os.environ.get('PUBLIC_KEY'),
        private_key=os.environ.get('PRIVATE_KEY')
    )
    return_data = {
        'action': 'pay',
        'amount': params.get('money_amount'),
        'currency': params.get('currency', 'UAH'),
        'description': params.get('description', ''),
        'order_id': params.get('order_id'),
        'version': '3',
        'sandbox': '0',  # sandbox mode, set to 1 to enable it
        'server_url': params.get('server_url'),  # url to callback view
        'language': 'ru'
    }
    
    if params.get('card'):
        return_data['card'] = params.get('card')
        return_data['tavv'] = params.get('tavv')

    data = {
        'signature': liqpay.cnb_signature(return_data),
        'data': liqpay.cnb_data(return_data)
    }

    return data
