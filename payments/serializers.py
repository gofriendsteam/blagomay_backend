import uuid
from rest_framework import serializers
from .models import Transaction


class TransactionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        validated_data['order_id'] = f'{uuid.uuid4()}'
        return Transaction.objects.create(**validated_data)

    class Meta:
        model = Transaction
        fields = ('full_name', 'email', 'phone_number',
                  'program', 'administrative_payments', 'is_regular',
                  'money_amount', 'currency', 'order_id')
        read_only = ('order_id',)


class ApplePayTransactionSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        validated_data['order_id'] = f'{uuid.uuid4()}'
        return Transaction.objects.create(**validated_data)

    class Meta:
        model = Transaction
        fields = ('full_name', 'email', 'phone_number',
                  'program', 'administrative_payments', 'is_regular',
                  'money_amount', 'currency', 'order_id', 'card', 'tavv')
        read_only = ('order_id',)

class WayForPaySerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['order_id'] = f'{uuid.uuid4()}'
        return Transaction.objects.create(**validated_data)

    class Meta:
        model = Transaction
        exclude  = ['order_id', 'card' , 'tavv' ,
                    'payment_beginning',
                    'payment_succeeded' , 'payment_method']


class DataSignatureSerializer(serializers.Serializer):
    signature = serializers.CharField(max_length=255)
    data = serializers.CharField(max_length=512)
