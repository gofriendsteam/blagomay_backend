SITE_UPDATE = 'на оновлення сайту'

USD = 'USD'
UAH = 'UAH'


class ProgramPaymentConstants:
    def __init__(self):
        pass

    PP_CHOICES = (
        (SITE_UPDATE, 'на оновлення сайту'),
    )
    CURRENCY_CHOICES = (
        (USD, 'USD'),
        (UAH, 'UAH')
    )


LiqPay = 'LiqPay'
Google = 'Google Pay'
Apple = 'Apple pay'

class TransactionPaymentTypes:
    def __init__(self, ):
        pass
    
    PAYMENT_TYPES = (
        (LiqPay, 'LiqPay'),
        (Google, 'Google Pay'),
        (Apple, 'Apple Pay'),
    )

PERCENT_20 = 20
PERCENT_15 = 15
PERCENT_10 = 10
PERCENT_5 = 5
PERCENT_0 = 0

class AdministrativePaymentsChoices:
    def __init__(self, ):
        pass
    
    PERCENT_VARIANTS = (
        (PERCENT_20, 20),
        (PERCENT_15, 15),
        (PERCENT_10, 10),
        (PERCENT_5, 5),
        (PERCENT_0, 0),
    )
