import os
import uuid
from liqpay.liqpay3 import LiqPay

from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from .serializers import (
    TransactionSerializer,
    DataSignatureSerializer,
    ApplePayTransactionSerializer,
    WayForPaySerializer)
from .utils import get_liqpay_data_and_signature
from .models import Transaction
from programs.models import ActiveProgram
from .constants import TransactionPaymentTypes


class PayView(CreateAPIView):
    serializer_class = TransactionSerializer

    def post(self, request, language):
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            # LiqPay stuff generation
            liqpay_data = get_liqpay_data_and_signature(
                request,
                **serializer.data,
            )

            return Response(data=liqpay_data, status=HTTP_201_CREATED)
        return Response({'err': True, 'msg': serializer.errors})

        def get(self, request):
            return Response({'error': True, 'msg': 'Post requeired'}, status=HTTP_403_FORBIDDEN)

        return Response(serializer.error_messages)


class ApplePayView(CreateAPIView):
    serializer_class = ApplePayTransactionSerializer

    def post(self, request, language):
        serializer = ApplePayTransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data, '\n\n\n\n')
            # LiqPay stuff generation
            liqpay_data = get_liqpay_data_and_signature(
                request,
                **serializer.data,
            )

            return Response(data=liqpay_data, status=HTTP_201_CREATED)
        return Response({'err':True, 'msg':serializer.errors})

        def get(self, request):
            return Response({'error':True, 'msg':'Post required'}, status=HTTP_403_FORBIDDEN)

        return Response(serializer.error_messages)


class LiqPayCallBackView(CreateAPIView):
    serializer_class = DataSignatureSerializer

    def post(self, request, *args, **kwargs):
        liqpay = LiqPay(
            public_key=os.environ.get('PUBLIC_KEY'),
            private_key=os.environ.get('PRIVATE_KEY')
        )
        serializer = DataSignatureSerializer(data=request.data)

        if not serializer.is_valid():
            return HttpResponse(serializer.errors)

        data = serializer.data['data']
        signature = serializer.data['signature']
        sign = liqpay.str_to_sign(os.environ.get('PUBLIC_KEY') + data + os.environ.get('PRIVATE_KEY'))

        response = liqpay.decode_data_from_str(data)
        print(response, '\n\n')
        if sign == signature:
            response['is_valid_signature'] = True

        transaction = Transaction.objects.get(order_id=response['order_id'])
        if response.get('status') == 'success':
            transaction.payment_succeeded = True
            transaction.save()
            return HttpResponse('ok')  # HttpResponseRedirect(redirect_to='https://blagomay.com')
        else:
            transaction.payment_succeeded = False
            transaction.save()

        return HttpResponse('not ok')


import requests
from django.contrib.sites.shortcuts import get_current_site
import datetime
from programs.models import ActiveProgram
import hmac


def hmac_md5(key, s):
    return hmac.new(key.encode('utf-8'), s.encode('utf-8'), 'MD5').hexdigest()


class WayForPayView(CreateAPIView):
    serializer_class = WayForPaySerializer

    def post(self, request, *args, **kwargs):
        serializer = WayForPaySerializer(data=request.data)
        if serializer.is_valid():
            new_order = serializer.save()
            try:
                program_id = serializer.data['program']
                current_product = ActiveProgram.objects.get(pk=program_id)
            except Exception as e:
                print(e)
            amount = serializer.data['money_amount']
            merchantSignature = "4a04c1be209b27544f4c43a0db4180446a7dbf53"
            merchantAccount = 'www_charitymay_com'
            domain_name = str(get_current_site(request))
            # data_send = requests.post('https://secure.wayforpay.com/pay', data={
            #     "merchantAccount": merchantAccount,
            #     'merchantDomainName': 'https://blago.charitymay.com/',
            #     'merchantSignature': merchantSignature,
            #     'orderReference': program_id,
            #     'orderDate': current_product.pub_date,
            #     'amount': amount,
            #     'currency': serializer.data['currency'],
            #     'productName[]': [current_product.title],
            #     'productPrice[]': [amount],
            #     'productCount[]': [1]
            # })
            # import time
            # import datetime
            # string =int(current_product.pub_date.timestamp())
            # # print()
            #
            # test_data = {
            #
            #     "merchantAccount": merchantAccount,
            #     "merchantAuthType": "SimpleSignature",
            #     "merchantDomainName": "https://blago.charitymay.com/",
            #     "merchantSignature": merchantSignature,
            #     # "apiVersion": 1,
            #     # "language": 'ru',
            #     # "serviceUrl": "https://blago.charitymay.com/",
            #     "orderReference": [new_order.id],
            #     "orderDate": string,
            #     "amount": amount,
            #     "currency": "UAH",
            #     "productName": [current_product.title],
            #     "productPrice": [float(amount)],
            #     "productCount": [1],
            #     # "clientFirstName": "Bulba",
            #     # "clientLastName": "Taras",
            #     # "clientEmail": serializer.data['email'],
            #     # "clientPhone": serializer.data['phone_number']
            # }
            # from pprint import pprint
            # pprint(test_data)
            # import json
            # headers = {'Content-type': 'application/json'}
            # response = requests.post('https://secure.wayforpay.com/pay',
            #                          json=test_data,
            #                          headers=headers)
            # try:
            #
            #     print(response.status_code)
            #     print(response.text)
            #     print(response)
            # except Exception as e:
            #     print(e)
            currency = serializer.data['currency']
            str_to_hash = "{account};" \
                          "{domain};" \
                          "{order_id};" \
                          "{order_date};" \
                          "{amount};" \
                          "{currency};" \
                          "{product_name}".format(
                account=merchantAccount,
                domain=domain_name,
                order_id=new_order.order_id,
                order_date=current_product.pub_date.timestamp(),
                amount=amount,
                currency=currency,
                product_name=current_product.title
            )
            hash_data = hmac_md5(key=merchantSignature, s=str_to_hash)
            print(hash_data)
            return Response({
                'hash_data_md5': hash_data,
                'merchantSignature': "4a04c1be209b27544f4c43a0db4180446a7dbf53",
                'merchantAccount': 'www_charitymay_com'
            })
        else:
            print('INValid!')
            print(serializer.errors)
            return Response({'err': True, 'msg': serializer.errors})
        # data = requests.post('https://api.wayforpay.com/api', data=obj)
        # print(data.text)
        print('WayForPayView')
        return HttpResponse(status=HTTP_201_CREATED)
