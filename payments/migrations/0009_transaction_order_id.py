# Generated by Django 2.1 on 2019-10-29 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0008_auto_20191029_0805'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='order_id',
            field=models.CharField(default='test_order_id', max_length=256),
            preserve_default=False,
        ),
    ]
