# Generated by Django 2.1 on 2019-07-16 13:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='start_payment',
        ),
    ]
