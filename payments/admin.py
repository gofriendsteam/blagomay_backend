from django.contrib import admin
from .models import Transaction, PaymentSummary

admin.site.register(Transaction)


@admin.register(PaymentSummary)
class PaymentSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'payment_summary.html'
    date_hierarchy = 'created'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )

        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        response.context_data['summary'] = list(
            qs.order_by('-created')
        )

        return response
