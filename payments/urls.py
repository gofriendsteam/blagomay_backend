from django.urls import path
from .views import PayView, LiqPayCallBackView, ApplePayView  , WayForPayView


urlpatterns = [
    path('pay/', PayView.as_view(), name='pay_url'),
    path('apple-pay/', ApplePayView.as_view(), name='apple_pay_url'),
    path('pay-callback/', LiqPayCallBackView.as_view(), name='pay_callback_url'),
    path('wayforpay/', WayForPayView.as_view(), name='wayforpay'),
]
