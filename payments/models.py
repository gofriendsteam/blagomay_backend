import uuid
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext as _
from .constants import ProgramPaymentConstants, AdministrativePaymentsChoices, TransactionPaymentTypes


class Transaction(models.Model):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order_id = models.CharField(max_length=256)
    email = models.CharField(max_length=100)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message=('Phone number must be entered in the format: "+380123456789". '
                'Up to 15 digits allowed.')
    )
    phone_number = models.CharField(
        validators=[phone_regex, ],
        max_length=17,
        blank=True,
        null=True
    )
    full_name = models.CharField(max_length=150)
    program = models.ForeignKey(
        'programs.ActiveProgram',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    payment_beginning = models.DateField(null=True, blank=True)
    payment_method = models.CharField(
        choices=TransactionPaymentTypes.PAYMENT_TYPES,
        max_length=100
    )
    money_amount = models.DecimalField(
        default=100,
        max_digits=5,
        decimal_places=2
    )
    currency = models.CharField(
        max_length=50,
        choices=ProgramPaymentConstants.CURRENCY_CHOICES
    )

    administrative_payments = models.IntegerField(
        choices=AdministrativePaymentsChoices.PERCENT_VARIANTS,
        null=True
    )
    is_regular = models.BooleanField(default=False)
    payment_succeeded = models.BooleanField(null=True)

    created = models.DateTimeField(auto_now_add=True)

    # apple pay stuff
    card = models.CharField(max_length=256, blank=True, null=True)
    tavv = models.CharField(max_length=256, blank=True, null=True)

    def money_amount_evaluated(self, ):
        if self.administrative_payments is True:
            return float(self.money_amount) * 0.8

        return float(self.money_amount)

    def administrative_costs_evaluated(self, ):
        if self.administrative_payments is True:
            return float(self.money_amount) * 0.2

        return 0.0

    class Meta:
        verbose_name = _("Payment information")
        verbose_name_plural = _("Payments information")

    def __str__(self):
        return '{} - payment'.format(self.full_name)


class PaymentSummary(Transaction):
    class Meta:
        proxy = True
        verbose_name = 'Payment Summary'
        verbose_name_plural = 'Payments Summary'
