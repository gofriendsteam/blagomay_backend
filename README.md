# Blagomay

## Installation
create virtual environment
```bash
python -m venv venv
```
activate venv
``` bash
source venv/bin/activate
```
Create database in postgres and user,
check db name and user from env.sh

1) DATABASE_NAME
2) DATABASE_USER
3) DATABASE_PASSWORD


run *env.sh* file
```bash
. env.sh
```

## Next
run migrate 
```bash
python manage.py migrate
```
install all packages from requirements.txt
```bash
pip install -r requirements/base.txt
```
run project
```bash
python manage.py runserver
```
