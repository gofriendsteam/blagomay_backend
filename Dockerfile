FROM python:3.7-alpine

WORKDIR /app

RUN apk add --update bash postgresql-dev gcc python3-dev musl-dev zlib-dev jpeg-dev git

COPY requirements.txt /app

RUN pip3 install -r requirements.txt

COPY . /app

EXPOSE 8080

CMD ["/bin/bash", "/app/start.sh"]