import datetime

from django.db import models
from django.core.validators import FileExtensionValidator
from django.utils.translation import ugettext as _

from .constants import TypeContentConstantsClass


class Reporting(models.Model):
    accounting_file = models.FileField(
        validators=[FileExtensionValidator(allowed_extensions=TypeContentConstantsClass.FILE_FORMAT)])
    title = models.CharField(max_length=255)
    description = models.TextField()
    
    date = models.PositiveSmallIntegerField(default=datetime.datetime.today().year)
    type_choice = models.CharField(max_length=100, choices=TypeContentConstantsClass.TYPE_CONTENT_CHOICES)
    
    class Meta:
        verbose_name = _("Accounting File")
        verbose_name_plural = _("Accounting Files")

    def __str__(self, ):
        return self.title

    def get_absolute_url(self):
        return self.accounting_file.url
