from django.urls import path
from .views import ReportingListAPIView


urlpatterns = [
    path('reporting/', ReportingListAPIView.as_view(), name='reporting_url'),
]
