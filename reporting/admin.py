import os

from django.contrib import admin, messages

from .models import Reporting
from .forms import ReportingModelForm
from .constants import TypeContentConstantsClass, FINANCE, YEAR, MONTH

EXCEL = TypeContentConstantsClass.FILE_FORMAT[0:3]
IMG = TypeContentConstantsClass.FILE_FORMAT[4:8]


class ReportingModelAdmin(admin.ModelAdmin):
    form = ReportingModelForm
    fields = ('date', 'accounting_file', 'title',
              'type_choice')

    def save_model(self, request, obj, form, change):
        filename, ext = os.path.splitext(obj.accounting_file.url)
        extension = ext[1:]

        if (obj.type_choice == FINANCE and extension not in EXCEL) \
                or (obj.type_choice == YEAR and extension not in IMG) \
                or (obj.type_choice == MONTH and extension not in IMG):

            for x in TypeContentConstantsClass.CONTENT_DICT.keys():
                if x == obj.type_choice:
                    messages.error(request, "Невірний формат для категорії, оберіть формат типів: {}".format(
                        TypeContentConstantsClass.CONTENT_DICT[x]))
            return

        super().save_model(request, obj, form, change)


admin.site.register(Reporting, ReportingModelAdmin)
