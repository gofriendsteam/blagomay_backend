import datetime
from django import forms


class ReportingModelForm(forms.ModelForm):
    YEAR_CHOICES = [(r, r) for r in range(2012, (datetime.datetime.now().year + 1))]
    date = forms.ChoiceField(choices=YEAR_CHOICES)
