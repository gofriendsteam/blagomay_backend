from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from reporting.models import Reporting
from .serializers import ReportingSerializer


class ReportingListAPIView(ListAPIView):
    permission_classes = (AllowAny, )
    serializer_class = ReportingSerializer
    queryset = Reporting.objects.all()
