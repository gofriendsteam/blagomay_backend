from houses.mixins import TranslatedModelSerializerMixin
from programs.models import ActiveProgram, PartnerOffer


class ActiveProgramSerializer(TranslatedModelSerializerMixin):
    class Meta:
        model = ActiveProgram
        fields = (
            'id',
            'title',
            'content',
            'pub_date',
            'image',
            'pay_limit',
            'paid_sum',
        )


class PartnerOfferSerializer(TranslatedModelSerializerMixin):
    class Meta:
        model = PartnerOffer
        fields = (
            'id',
            'content',
            'image',
        )
