from ckeditor.fields import RichTextField
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext as _


class ActiveProgram(models.Model):
    title = models.CharField(null=True, blank=True, max_length=1000, verbose_name=_('Title'))
    content = RichTextField(null=True, blank=True, verbose_name=_('Content'))
    pub_date = models.DateTimeField(default=now, verbose_name=_('Publication date'))
    image = models.ImageField(upload_to='programs/images/', blank=True, null=True, verbose_name=_('Active program image'))
    pay_limit = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True, verbose_name=_('Limit'))
    paid_sum = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True, verbose_name=_('Paid'))

    class Meta:
        verbose_name = _('Active program')
        verbose_name_plural = _('Active programs')

    def __str__(self, ):
        return self.title


class PartnerOffer(models.Model):
    content = models.CharField(
        null=True,
        blank=True,
        max_length=200,
        verbose_name=_("Partner's offer"),
    )
    image = models.ImageField(
        upload_to='programs/partners_offers_images/',
        blank=True,
        null=True,
        verbose_name=_("Partner's offer image"),
    )
