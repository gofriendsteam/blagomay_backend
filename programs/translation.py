from modeltranslation.translator import translator, TranslationOptions
from django.utils.translation import ugettext as _

from programs.models import ActiveProgram, PartnerOffer


class ActiveProgramTranslate(ActiveProgram):
    class Meta:
        proxy = True
        verbose_name = _('Active program origin')
        verbose_name_plural = _('Active programs origin')


class ActiveProgramTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )


class PartnerOfferTranslate(PartnerOffer):
    class Meta:
        proxy = True
        verbose_name = _("Partner's offer origin")
        verbose_name_plural = _("Partners' offers origin")


class PartnerOfferTranslateOptions(TranslationOptions):
    fields = ('content', )


translator.register(ActiveProgram, ActiveProgramTranslationOptions)
translator.register(PartnerOffer, PartnerOfferTranslateOptions)
