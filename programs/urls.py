from django.urls import path

from programs.views import (
    ActiveProgramListView,
    ActiveProgramRetrieveView,
    PartnerOfferRetrieveView,
    ActiveProgramSearchListAPIView,
    APageSearchListAPIView,
)

app_name = 'programs'

urlpatterns = [
    path('programs/', ActiveProgramListView.as_view(), name='program_list'),
    path('programs/<int:pk>', ActiveProgramRetrieveView.as_view(), name='program_retrieve'),
    path('programs/partneroffer/<int:pk>', PartnerOfferRetrieveView.as_view(), name='partner_offer_retrieve'),
    path('programs/search/', ActiveProgramSearchListAPIView.as_view(), name='program_search'),
    path('programs/search/result/', APageSearchListAPIView.as_view())
]
