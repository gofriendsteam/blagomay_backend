from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView, RetrieveAPIView

from programs.models import ActiveProgram, PartnerOffer
from programs.serializers import (
    ActiveProgramSerializer,
    PartnerOfferSerializer,
)
from news.backends import SearchSQLFilter


class ActiveProgramListView(ListAPIView):
    queryset = ActiveProgram.objects.all()
    serializer_class = ActiveProgramSerializer


class ActiveProgramRetrieveView(RetrieveAPIView):
    queryset = ActiveProgram.objects.all()
    serializer_class = ActiveProgramSerializer


class PartnerOfferRetrieveView(RetrieveAPIView):
    queryset = PartnerOffer.objects.all()
    serializer_class = PartnerOfferSerializer


class ActiveProgramSearchListAPIView(ListAPIView):
    queryset = ActiveProgram.objects.all()
    serializer_class = ActiveProgramSerializer
    filter_backends = [SearchSQLFilter]
    search_fields = ['title']
    pagination_class = None


class APageSearchListAPIView(ListAPIView):
    queryset = ActiveProgram.objects.all()
    serializer_class = ActiveProgramSerializer
    filter_backends = [SearchFilter]
    search_fields = ['title']
