from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from programs.models import ActiveProgram, PartnerOffer
from programs.translation import (
    ActiveProgramTranslate,
    PartnerOfferTranslate,
)


@admin.register(ActiveProgram)
class ActiveProgramTranslateAdmin(TranslationAdmin):
    pass


@admin.register(ActiveProgramTranslate)
class ActiveProgramAdmin(admin.ModelAdmin):
    fields = (
        'title',
        'content',
        'image',
        'pub_date'
    )


@admin.register(PartnerOffer)
class PartnerOfferTranslateAdmin(TranslationAdmin):
    pass


@admin.register(PartnerOfferTranslate)
class PartnerOfferAdmin(admin.ModelAdmin):
    fields = (
        'content',
        'image',
    )
