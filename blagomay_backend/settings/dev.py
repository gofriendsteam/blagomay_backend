from .base import *

DEBUG = True

ELASTICSEARCH_INDEX_NAMES = {
    'search_indexes.documents.news': 'news',
}

ELASTICSEARCH_DSL = {
    'default': {
        'hosts': 'localhost:9200'
    },
}
