from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from drf_yasg.views import get_schema_view

from rest_framework.permissions import AllowAny

from blagomay_backend import settings


class CategorizedAutoSchema(SwaggerAutoSchema):
    def get_tags(self, operation_keys):
        if len(operation_keys) >= 1:
            operation_keys = operation_keys[1:]
        return super().get_tags(operation_keys)


schema_view = get_schema_view(
    openapi.Info(
      title="Blagomay Project API",
      default_version='v1',
    ),
    permission_classes=(AllowAny,),
)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/docs/', schema_view.with_ui(), name='docs'),
    path('api/<str:language>/', include('news.urls')),
    path('api/<str:language>/', include('houses.urls')),
    path('api/<str:language>/', include('programs.urls')),
    path('api/<str:language>/', include('about.urls')),
    path('api/<str:language>/', include('reporting.urls')),
    path('api/<str:language>/pay/', include('payments.urls')),

] \
    + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
    + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
