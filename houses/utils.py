# -*- coding: utf-8 -*-
from datetime import datetime
from houses.models import ChildrenHouse, UkrainianRegion

try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree


def parseXML(xml_file='houses/wordpress.2019-07-16.xml'):
    with open(xml_file, 'rb') as f:
        xml = f.read()
        root = etree.fromstring(xml)
        for channel in root.getchildren():
            item_dict = {}
            for i, elem in enumerate(channel.getchildren()):
                if elem.tag == 'item':
                    instance_dict = {}
                    for house in elem.getchildren():
                        if house.tag == '{http://purl.org/rss/1.0/modules/content/}encoded':
                            instance_dict['content'] = house.text or None
                        if house.tag == 'title':
                            instance_dict['title'] = house.text or None
                        if house.tag == 'pubDate':
                            instance_dict['pubDate'] = house.text or None
                        if house.tag == 'category':
                            if '-region' in house.attrib.get('nicename'):
                                instance_dict['region'] = house.text or None

                    item_dict[i] = instance_dict
                    print(instance_dict)
        for key in item_dict.keys():
            item = item_dict[key]
            pub_date = datetime.strptime(item['pubDate'], "%a, %d %b %Y %H:%M:%S %z")
            if item.get('region'):
                region, _ = UkrainianRegion.objects.get_or_create(name_ru=item.get('region'))
                print(region)

                ChildrenHouse.objects.create(
                    region=region,
                    pub_date=pub_date,
                    title_ru=item['title'],
                    content_ru=item['content'],
                    is_archive=True,
                )
    return


if __name__ == "__main__":
    parseXML('houses/wordpress.2019-07-16.xml')
