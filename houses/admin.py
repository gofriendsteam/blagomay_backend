from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from django.utils.translation import ugettext as _
from houses.models import UkrainianRegion, ChildrenHouse, ChildrenNeed


# ukrainian region
from houses.translation import ChildrenHouseProxy, ChildrenNeedProxy, UkrainianRegionProxy


@admin.register(UkrainianRegion)
class RegionAdminTranslate(TranslationAdmin):
    pass


@admin.register(UkrainianRegionProxy)
class UkrainianRegionAdmin(admin.ModelAdmin):
    fields = (
        'name',
    )


# children house with needs


class ChildrenHouseTranslateTabularInline(TranslationTabularInline):
    model = ChildrenNeed
    extra = 1


@admin.register(ChildrenHouse)
class ChildrenHouseTranslateAdmin(TranslationAdmin):
    inlines = (ChildrenHouseTranslateTabularInline, )


class ChildrenNeedTabularInline(admin.TabularInline):
    model = ChildrenNeedProxy
    extra = 1


@admin.register(ChildrenHouseProxy)
class ChildrenHouseAdmin(admin.ModelAdmin):
    fields = (
        'title',
        'content',
        'image',
        'region',
        'pub_date'
    )
    inlines = (ChildrenNeedTabularInline, )
