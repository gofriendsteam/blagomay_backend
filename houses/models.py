from ckeditor.fields import RichTextField
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext as _


class UkrainianRegion(models.Model):
    name = models.CharField(max_length=256, verbose_name=_('Name'),
                            null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Ukrainian region')
        verbose_name_plural = _('Ukrainian regions')

    @property
    def children_houses_count(self):
        return self.children_houses.count()


class ChildrenHouse(models.Model):
    title = models.CharField(null=True, blank=True, max_length=1000, verbose_name=_('Title'))
    content = RichTextField(null=True, blank=True, verbose_name=_('Content'))
    image = models.ImageField(
        upload_to='houses/children_houses/images/',
        blank=True, null=True,
        verbose_name=_('Children house image')
    )
    region = models.ForeignKey(
        UkrainianRegion, on_delete=models.SET_NULL,
        null=True, blank=True,
        related_name='children_houses'
    )
    pub_date = models.DateTimeField(default=now, verbose_name=_('Publication date'))
    is_archive = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Children house')
        verbose_name_plural = _('Children houses')


class ChildrenNeed(models.Model):
    children_house = models.ForeignKey(ChildrenHouse, on_delete=models.CASCADE, related_name='children_needs')
    name = models.CharField(max_length=255, verbose_name=_('Name of needs (UA)'), null=True, blank=True)

    class Meta:
        verbose_name = _('Children house need')
        verbose_name_plural = _('Children house needs')

