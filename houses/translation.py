from modeltranslation.translator import translator, TranslationOptions
from django.utils.translation import ugettext as _

from houses.models import UkrainianRegion, ChildrenHouse, ChildrenNeed

# proxy models


class UkrainianRegionProxy(UkrainianRegion):
    class Meta:
        proxy = True
        verbose_name_plural = _('Ukrainian regions origin')
        verbose_name = _('Ukrainian region origin')


class ChildrenHouseProxy(ChildrenHouse):
    class Meta:
        proxy = True
        verbose_name_plural = _('Children houses origin')
        verbose_name = _('Children house origin')


class ChildrenNeedProxy(ChildrenNeed):
    class Meta:
        proxy = True
        verbose_name_plural = _('Children needs origin')
        verbose_name = _('Children need origin')


class UkrainianRegionTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(UkrainianRegion, UkrainianRegionTranslationOptions)
translator.register(UkrainianRegionProxy)


class ChildrenHouseTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )


translator.register(ChildrenHouse, ChildrenHouseTranslationOptions)
translator.register(ChildrenHouseProxy)


class ChildrenNeedTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(ChildrenNeed, ChildrenNeedTranslationOptions)
translator.register(ChildrenNeedProxy)
