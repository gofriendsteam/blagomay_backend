from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView

from houses.models import UkrainianRegion, ChildrenHouse
from houses.serializers import RegionUASerializer, ChildrenHouseSerializer, ChildrenNeedSerializer
from houses.utils import parseXML


class RegionListView(ListAPIView):
    queryset = UkrainianRegion.objects.all()
    serializer_class = RegionUASerializer


class ChildrenHousesByRegion(RetrieveAPIView):
    queryset = ChildrenHouse.objects.all()
    serializer_class = ChildrenHouseSerializer


class ChildrenMigrateAPIView(APIView):
    def get(self, request, language='ru'):
        parseXML()
        return Response(status=201)


class ChildrenHouseListAPIView(ListAPIView):
    queryset = ChildrenHouse.objects.all()
    serializer_class = ChildrenHouseSerializer
    pagination_class = None
