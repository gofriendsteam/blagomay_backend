from django.urls import path

from houses.views import RegionListView, ChildrenHousesByRegion, ChildrenMigrateAPIView, ChildrenHouseListAPIView

app_name = 'houses'

urlpatterns = [
    path('regions/', RegionListView.as_view(), name='regions_list'),
    path('house_by_region/<int:pk>/', ChildrenHousesByRegion.as_view(), name='house_by_region'),
    path('migrate/houses/', ChildrenMigrateAPIView.as_view(), name='migrate_url'),
    path('children/houses/', ChildrenHouseListAPIView.as_view(), name='houses_url')
]
