from rest_framework import serializers

from houses.mixins import TranslatedModelSerializerMixin
from houses.models import UkrainianRegion, ChildrenHouse, ChildrenNeed


class RegionUASerializer(TranslatedModelSerializerMixin):
    class Meta:
        model = UkrainianRegion
        fields = (
            'id',
            'name',
            'children_houses_count',
        )


class ChildrenNeedSerializer(TranslatedModelSerializerMixin):
    class Meta:
        model = ChildrenNeed
        fields = (
            'id',
            'name',
        )


class ChildrenHouseSerializer(TranslatedModelSerializerMixin):
    needs = ChildrenNeedSerializer(many=True, source='children_needs')
    region = serializers.CharField(source='region.name', read_only=True)

    class Meta:
        model = ChildrenHouse
        fields = (
            'id',
            'title',
            'content',
            'pub_date',
            'region',
            'needs'
        )
