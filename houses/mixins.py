from django.conf import settings
from modeltranslation.manager import get_translatable_fields_for_model
from rest_framework import serializers


class TranslatedModelSerializerMixin(serializers.ModelSerializer):
    def get_field_names(self, declared_fields, info):
        fields = super().get_field_names(declared_fields, info)
        trans_fields = get_translatable_fields_for_model(self.Meta.model) or []
        all_fields = []

        requested_langs = []
        if 'request' in self.context:
            lang_param = self.context['view'].kwargs.get('language', None)
            requested_langs = lang_param.split(',') if lang_param else []
        for f in fields:
            if f not in trans_fields:
                all_fields.append(f)
            else:
                for l in settings.LANGUAGES:
                    if not requested_langs or l[0] in requested_langs:
                        all_fields.append("{}_{}".format(f, l[0]))

        return all_fields

    def to_representation(self, instance):
        data = dict(super().to_representation(instance))
        lang = self.context['view'].kwargs.get('language', None)
        for key, value in data.items():
            if key[-3:] == '_' + lang:
                data[key[:-3]] = data.pop(key)
        return data

