#!/usr/bin/env bash
echo "====== RUN MIGRATIONS ======"
python3 manage.py migrate

echo "====== COLLECT STATIC ======"
python3 manage.py collectstatic -c --noinput

echo "====== RUN APP ======"
gunicorn -b 0.0.0.0:8080 --workers=4 --threads=3 --max-requests=0 blagomay_backend.wsgi