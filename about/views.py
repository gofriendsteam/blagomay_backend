from rest_framework.generics import ListAPIView

from about.models import Achievement
from about.serializers import AchievementSerializer


class AchievementListView(ListAPIView):
    queryset = Achievement.objects.all()
    serializer_class = AchievementSerializer
