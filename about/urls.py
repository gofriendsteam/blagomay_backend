from django.urls import path

from about.views import AchievementListView

app_name = 'about'


urlpatterns = [
    path('about/achievements/', AchievementListView.as_view(), name='achievement_view'),
]
