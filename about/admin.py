from django.contrib import admin
from about.models import Achievement
from modeltranslation.admin import TranslationAdmin
from about.translation import AchievementTranslate


class AchievementTranslateAdmin(TranslationAdmin):
    pass


admin.site.register(Achievement, AchievementTranslateAdmin)


@admin.register(AchievementTranslate)
class AchievementAdmin(admin.ModelAdmin):
    fields = (
        'content',
        'img',
    )
