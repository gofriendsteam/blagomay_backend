from django.db import models


class Achievement(models.Model):
    content = models.TextField()
    img = models.ImageField(
        upload_to='about/achievements_images/',
        blank=True,
        null=True,
    )
