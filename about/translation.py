from modeltranslation.translator import translator, TranslationOptions
from about.models import Achievement
from django.utils.translation import ugettext as _


class AchievementTranslate(Achievement):
    class Meta:
        proxy = True
        verbose_name = _('Achievement origin')
        verbose_name_plural = _('Achievements origin')


class AchievementTranslationOptions(TranslationOptions):
    fields = ('content', )


translator.register(Achievement, AchievementTranslationOptions)
translator.register(AchievementTranslate)
