from houses.mixins import TranslatedModelSerializerMixin
from about.models import Achievement


class AchievementSerializer(TranslatedModelSerializerMixin):

    class Meta:
        model = Achievement
        fields = (
            'content',
            'img',
        )
