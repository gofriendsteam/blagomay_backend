from django.contrib import admin
from news.models import News, NewsImage, Video
from modeltranslation.admin import TranslationAdmin
from news.translation import NewsTranslate


class NewsImageAdmin(admin.StackedInline):
    model = NewsImage


class NewsTranslateAdmin(TranslationAdmin):
    inlines = [NewsImageAdmin, ]


admin.site.register(News, NewsTranslateAdmin)


@admin.register(NewsTranslate)
class NewsAdmin(admin.ModelAdmin):
    fields = (
        'title',
        'content',
        'image',
        'pub_date',
        'program',
    )

admin.site.register(Video)
