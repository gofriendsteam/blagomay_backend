from modeltranslation.translator import translator, TranslationOptions
from news.models import News
from django.utils.translation import ugettext as _


class NewsTranslate(News):
    class Meta:
        proxy = True
        verbose_name = _('News origin')
        verbose_name_plural = _('News origin')


class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'content', )


translator.register(News, NewsTranslationOptions)
translator.register(NewsTranslate)
