from rest_framework.filters import SearchFilter


class SearchSQLFilter(SearchFilter):
    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)
        language = get_language(request)

        if not search_fields or not search_terms:
            return queryset
        Model = queryset.model

        field_one = search_fields[0] + f'_{language}'
        field_two = f'%{search_terms[0]}%'
        where_expression = f"{field_one} LIKE %s LIMIT 5;"
        queryset = Model.objects.raw(
            f"""SELECT * FROM {Model._meta.db_table} WHERE """ + where_expression, params=[field_two]
        )
        return tuple(queryset)


def get_language(request):
    path = request.get_full_path()
    if '/en/' in path:
        return 'en'
    if '/ru/' in path:
        return 'ru'
    if '/uk/' in path:
        return 'uk'
