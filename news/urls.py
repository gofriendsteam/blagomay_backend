from __future__ import (absolute_import, division, print_function, unicode_literals)

from django.urls import path

from news.views import (
    NewsListView,
    NewsDetailView,
    NewsArchiveView,
    NewsArchiveDetailView,
    GetOldNewsAPIView,
    NewsSearchListAPIView,
    NewsSearchPageListAPIView,
)

app_name = 'news'

urlpatterns = [
    path('news/', NewsListView.as_view(), name='news_list'),
    path('news/<int:pk>', NewsDetailView.as_view(), name='news_detail'),
    path('archive_news/', NewsArchiveView.as_view(), name='news_archive_list'),
    path('old_news/', GetOldNewsAPIView.as_view(), name='old_news_archive_list'),
    path('archive_news/<int:pk>/', NewsArchiveDetailView.as_view(), name='news_archive_detail'),
    path('news/search/', NewsSearchListAPIView.as_view(), name='search_news_list'),
    path('news/search/result/', NewsSearchPageListAPIView.as_view(), name='search_result_list')
]
