from ckeditor.fields import RichTextField
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext as _


class NewsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_archive=False)


class NewsArchiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_archive=True)


class NewsImage(models.Model):
    image = models.ImageField(upload_to='news/images/', blank=True, null=True, verbose_name=_('News image'))
    alt = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('Alt'))
    news = models.ForeignKey('news.News', verbose_name=_("News"), on_delete=models.CASCADE)

    def get_absolute_url(self):
        return self.image.url


class News(models.Model):
    title = models.CharField(null=True, blank=True, max_length=1000, verbose_name=_('Title'))
    content = RichTextField(null=True, blank=True, verbose_name=_('Content'))
    pub_date = models.DateTimeField(default=now, verbose_name=_('Publication date'), null=True, blank=True)
    is_archive = models.BooleanField(default=False, verbose_name=_('In Archive'))
    program = models.ForeignKey(
        "programs.ActiveProgram",
        null=True,
        verbose_name=_("Program"),
        on_delete=models.CASCADE
    )

    objects = NewsManager()
    archive_objects = NewsArchiveManager()


    def __str__(self):
        return self.title



    class Meta:
        verbose_name = _('News')
        verbose_name_plural = _('News')
        ordering = ['-id']


class Video(models.Model):
    video = models.FileField(upload_to='video_archive/', blank=False, null=False, verbose_name=_('Video'))
    title = models.CharField(max_length=150)
    content = models.CharField(max_length=500)

    class Meta:
        verbose_name = _('Videos')
        verbose_name_plural = _('Videos archive')


class ImportantNumbers(models.Model):
    nubmber = models.IntegerField()
    title = models.CharField(max_length=200)

    class Meta:
        verbose_name = _('Important number')
        verbose_name_plural = _('Important numbers')
