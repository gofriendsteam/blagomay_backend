from rest_framework import serializers

from houses.mixins import TranslatedModelSerializerMixin
from news.models import News, Video, NewsImage


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsImage
        fields = ['get_absolute_url',]


class NewsSerializer(TranslatedModelSerializerMixin):
    images = ImageSerializer(source='newsimage_set', many=True)

    class Meta:
        model = News
        fields = (
            'id',
            'title',
            'content',
            'pub_date',
            'images',
        )


class VideoSerializer(TranslatedModelSerializerMixin):
    class Meta:
        model = Video
        fields = (
            'id',
            'title',
            'content',
            'video',
        )
