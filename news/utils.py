# -*- coding: utf-8 -*-
from datetime import datetime
from pprint import pprint
from django.utils.dateparse import parse_datetime
from news.models import News

try:
    from lxml import etree
except ImportError:
    import xml.etree.ElementTree as etree


def parseXML(xml_file='news/wordpress.2019-06-10.xml'):
    with open(xml_file, 'rb') as f:
        xml = f.read()
        root = etree.fromstring(xml)
        for channel in root.getchildren():
            item_dict = {}
            for i, elem in enumerate(channel.getchildren()):
                if elem.tag == 'item':
                    instance_dict = {}
                    for news in elem.getchildren():
                        if news.tag == '{http://purl.org/rss/1.0/modules/content/}encoded':
                            instance_dict['content'] = news.text or None
                        if news.tag == 'title':
                            instance_dict['title'] = news.text or None
                        if news.tag == 'pubDate':
                            instance_dict['pubDate'] = news.text or None
                        if news.tag == 'description':
                            instance_dict['description'] = news.text or None
                    item_dict[i] = instance_dict
    for key in item_dict.keys():
        item = item_dict[key]
        # print(parse_datetime(item['pubDate']))
        pub_date = datetime.strptime(item['pubDate'], "%a, %d %b %Y %H:%M:%S %z")
        print(type(pub_date))
        news = News.objects.create(
            pub_date=pub_date,
            title_ru=item['title'],
            content_ru=item['content'],
            is_archive=True,
        )
        print(news)
    return


if __name__ == "__main__":
    parseXML('wordpress.2019-06-10.xml')

