from __future__ import (absolute_import, division, print_function, unicode_literals)

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework.views import APIView

from news.models import News, Video
from news.serializers import NewsSerializer, VideoSerializer

from .utils import parseXML
from .backends import SearchSQLFilter


class NewsListView(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer


class NewsDetailView(RetrieveAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer


class NewsArchiveView(ListAPIView):
    queryset = News.archive_objects.all()
    serializer_class = NewsSerializer


class NewsArchiveDetailView(RetrieveAPIView):
    queryset = News.archive_objects.all()
    serializer_class = NewsSerializer


class GetOldNewsAPIView(APIView):
    def get(self, request, language='ru'):
        parseXML()
        return Response(status=201)


class NewsSearchListAPIView(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    filter_backends = [SearchSQLFilter]
    search_fields = ['title']
    pagination_class = None


class NewsSearchPageListAPIView(ListAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    filter_backends = [SearchFilter]
    search_fields = ['title']
