# Generated by Django 2.1 on 2019-06-18 18:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0004_news_pub_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='news/images/', verbose_name='News image'),
        ),
    ]
