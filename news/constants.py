
class LANGUAGES:
    ENGLISH = 'en'
    UKRAINIAN = 'ua'
    RUSSIAN = 'ru'

    LANGUAGES_TYPE = (
        (ENGLISH, 'English'),
        (UKRAINIAN, 'Ukrainian'),
        (RUSSIAN, 'Russian')
    )

